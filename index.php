<?php
require 'vendor/autoload.php';

use Beeblebrox3\Html\Html;

$url = 'http://www.google.com';
$target = 'blank';

echo Html::create('a', array(
    'href' => $url,
    'target' => $target
))->setContent('Google');

echo HTML::create('meta');