<?php

namespace Beeblebrox3\Html;

class Html {
    protected $markup = '';
    protected $attrs = array();
    protected $content = '';
    protected $close_required = true;
    protected $tag = '';

    /**
     * 
     * @param string $tag tagname
     * @param array $attrs attributes
     * @throws Exception
     */
    public function __construct($tag, $attrs = array()) {
        if (!is_string($tag)) {
            throw new Exception ('$tag MUST be string');
        }

        $this->tag = $tag;

        if ($attrs && !is_array($attrs)) {
            throw new Exception ('$attrs MUST be associative array');
        }

        if ($attrs) {
            foreach ($attrs as $attr => $value) {
                $this->setAttr($attr, $value);
            }
        }
    }

    /**
     * Returns a new instance
     * @param string $tag tagname
     * @param array $attrs attributes
     * @return \Beeblebrox3\Html\Html
     */
    public static function create($tag, $attrs = array()) {
        return new Html($tag, $attrs);
    }

    /**
     * Returns the HTML of this object and children
     * @param mixed $content a text content or other object
     * @return string
     */
    public function build ($content = null) {
        $this->applyFilters();
        
        if (!is_null($content)) {
            try {
                $this->setContent($content);
            } catch (Exception $e) {
            }
        }

        $this->markup = "<{$this->tag}";
        if ($this->attrs) {
            $attrs = array();
            foreach ($this->attrs as $attr => $value) {
                $attrs[] = "{$attr}=\"{$value}\"";
            }
            $this->markup .= ' ' . implode(' ', $attrs);
        }

        if (!$this->content && !$this->close_required) {
            $this->markup .= ' />';
        } else {
            $this->markup .= '>';

            if ($this->content) {
                $this->markup .= $this->content;
            }

            $this->markup .= "</{$this->tag}>";
        }

        return $this->markup;
    }

    /**
     * @param mixed $attr attribute name or associative array with attrs and values
     * @param mixed $value string, float or int  (required if $attr is a string)
     * @return \Beeblebrox3\Html\Html
     */
    public function setAttr($attr, $value = '') {
        if (is_array($attr)) {
            foreach ($attr as $_attr => $value) {
                $this->setAttr($_attr, $value);
            }
        } elseif (is_string($attr) && (is_numeric($value) || is_string($value))) {
            $this->attrs[$attr] = $value;
        }

        return $this;
    }

    /**
     * set the content of this object
     * @param mixed $content object or string
     * @return \Beeblebrox3\Html\Html
     * @throws InvalidArgumentException
     */
    public function setContent($content) {
        if (is_string($content)) {
            $this->content .= $content;
        } elseif (is_object($content) && method_exists($content, 'build')) {
            $this->content .= $content->build();
        } else {
            throw new InvalidArgumentException('$content MUST be string or Object');
        }

        return $this;
    }
    
    public function __toString() {
        return $this->build();
    }

    /**
     * [applyFilters description]
     */
    private function applyFilters() {
        $this->isCloseRequired();
    }

    /**
     * determine if tag must to be closed with (</tag>) or just />
     */
    private function isCloseRequired() {
        $notRequired = array (
            'link',
            'meta',
        );

        if (in_array($this->tag, $notRequired)) {
            $this->close_required = false;
        }
    }
}