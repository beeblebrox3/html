<?php

namespace Beeblebrox3\Html;

class Table extends Html {

    private $theader = null;
    private $tbody = null;

    public function __construct($attrs = array()) {
        parent::__construct('table', $attrs);
    }
}